//css样式变量
export class cssVariables{

  //消息框样式
  public msgBgSuccess="#1c2518"
  public msgBorderSuccess="#25371c"
  public msgFontSuccess="#67c23a"

  public msgBgWarning="#292218"
  public msgBorderWarning="#3e301c"
  public msgFontWarning="#e6a23c"

  public msgBgMessage="#202121"
  public msgBorderMessage="#2d2d2f"
  public msgFontMessage="#909399"

  public msgBgError="#2b1d1d"
  public msgBorderError="#412626"
  public msgFontError="#f56c6c"
}