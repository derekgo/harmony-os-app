//@Time : 2024-01-18 16:48
//@Author : 沉默小管
//@File : index.ets
//@web  : https://blog.csdn.net/qq_36977923
//@git  : https://gitee.com/derekgo
//@Software: DevEco Studio

export type methodsData = "post" | "get"
interface downloadParamsInterface{
  url: string
  params?: any
  filename: string
  isPost?: boolean
}
export interface configInterface{
  url: string
  data?: any
  method?: methodsData
  headers?: any
  downloadData?:downloadParamsInterface
}
export interface responseInterface {
  data:any
  message:string
  code:Number
}